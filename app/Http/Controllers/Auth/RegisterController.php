<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\Auth\RegisterRequest;
use App\Models\User;
use Illuminate\Support\Facades\Log;

class RegisterController extends Controller
{
    /**
     * Show the form for the user registration.
     *
     * @return \Illuminate\View\View
     */
    public function register()
    {
        return view('admin.register');
    }

    /**
     * Handle posting of the form for the user registration.
     *
     * @param RegisterRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function processRegistration(RegisterRequest $request)
    {
        DB::beginTransaction();
        try {
            $maxId = User::leftJoin('role_users AS ru', 'user_id', 'users.id')
                ->where('ru.role_id', 4)
                ->with('roles', 'activations')
                ->count() + 1;
            $userCode = 'SV';
            if ($maxId < 100) {
                $maxId += 100;
                $userCode = $userCode . '0' . substr($maxId, 1);
            } else
                $userCode = $userCode . $maxId;
            $request->offsetSet('created_by', $request['email']);
            $request->offsetSet('updated_by', $request['email']);
            $request->offsetSet('code', $userCode);

            if ($user = Sentinel::register($request->all())) {
                $activation = Activation::create($user);

                //Attach the user to the role
                $role = Sentinel::findRoleBySlug('student');
                $role->users()
                    ->attach($user);

                $code = $activation->code;
                // dd(compact('user', 'code'));
                Mail::send(
                    'admin.auth.emails.activate',
                    compact('user', 'code'),
                    function ($m) use ($user) {
                        return $m->to($user->email)->subject('Activate Your Account');
                    }
                );

                DB::commit();

                Session::flash('success', __('Đăng ký thành công'));

                return redirect()->route('login.form');
            }
        } catch (\Exception $exception) {
            DB::rollBack();

            Log::info($exception->getFile() . $exception->getLine() . ':' . $exception->getMessage());
            Session::flash('failed', __('Đăng ký thất bại'));

            return redirect()->route('register.form');
        }
    }

    /**
     * Handle activation for the user registration.
     *
     * @param int $userId
     * @param string $code
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function activate($userId, $code)
    {
        $user = Sentinel::findById($userId);

        if (Activation::complete($user, $code)) {

            // Activation was successfull
            Session::flash('success', __('auth.activate_successful'));

            return redirect()->route('login.form');
        } else {
            Activation::removeExpired();
            // Activation not found or not completed.
            Session::flash('failed', __('auth.activate_unsuccessful'));

            return redirect()->route('register.form');
        }
    }
}
