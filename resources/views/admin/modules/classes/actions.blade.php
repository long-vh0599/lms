<a href="{{ route('class.show', [$row->id]) }}" class="btn btn-info" title="Xem chi tiết lớp học">
    <i class="far fa-eye"></i>
</a>
<a href="{{ route('class.edit', [$row->id]) }}" class="btn btn-success" title="Chỉnh sửa thông tin lớp học">
    <i class="fas fa-edit"></i>
</a>
<a href="#" class="btn btn-danger" data-toggle="modal" data-target="#modal-sm"
    onclick="javascript:class_delete({{ $row->id }})" title="Xóa lớp học">
    <i class="far fa-trash-alt"></i>
</a>
<a href="{{ route('students') }}" class="btn btn-primary" title="Xem học viên">
    <i class="fas fa-users"></i>
</a>
<a href="{{ route('score.index') }}" class="btn btn-warning" title="Xem điểm bài kiểm tra">
    <i class="bi bi-journal-text"></i>
</a>
<!-- @if ($row->users_count > 0)
    <button class='btn' style="background-color: orange; color: white;" title="Học viên đang chờ phê duyệt">
        {{ $row->users_count }}
    </button>
@endif -->