<a href="{{ route('course.detail', [$row->id]) }}" class="btn btn-info" title="Thông tin khóa học">
    <i class="far fa-eye"></i>
</a>
<a href="{{ route('course.edit', [$row->id]) }}" class="btn btn-success" title="Sửa thông tin khóa học">
    <i class="fas fa-edit"></i>
</a>
<a class="btn btn-danger" title="Xóa khóa học" data-toggle="modal" data-target="#deleteModal"
    onclick="javascript:course_delete('{{ $row->id }}')">
    <i class="far fa-trash-alt"></i>
</a>
